import React from 'react';
import ReactDOM from 'react-dom';
// CSS
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css'
import '../../node_modules/font-awesome/css/font-awesome.min.css'
import '../../node_modules/slick-carousel/slick/slick.css';
import '../../node_modules/slick-carousel/slick/slick-theme.css';
import '../css/styles.css'

// Javascript
import '../../node_modules/jquery/dist/jquery.min.js'
import '../../node_modules/popper.js/dist/popper.min.js'
import '../../node_modules/bootstrap/dist/js/bootstrap.min.js'
import '../../node_modules/slick-carousel/slick/slick.min.js'

// Components
import { Header } from './components/Header';
import { Home } from './components/Home';
import { About } from './components/About';
import { Dowedo } from './components/Dowedo';
import { Who } from './components/Who/Who';
import { Clients } from './components/Clients';
import { Havewedone } from './components/Havewedone';
import { DigitalDesign } from './components/DigitalDesign';
import { Branding } from './components/Branding';
import { Photography } from './components/Photography';
import { Advertising } from './components/Advertising';
import { Video } from './components/Video';
import { Contact } from './components/Contact';
import { Menu } from './components/Menu';
import { Cloud } from './components/nature/Cloud'

class App extends React.Component {

    constructor() {
        super();
        this.state = {
           
        }
    }

    fallDown(e, elem) {
        e.preventDefault();
        Jump(elem, {
            duration: 1000
          })  
    }

    bootstrapPreloader() {
        const preloaderElem = document.querySelector('.preloader');
        console.log("preloader", preloaderElem)

        setTimeout(() => {
            preloaderElem.classList.add('invisible');
        }, 3000);

        // preloaderElem.remove();
    }

    componentDidMount() {
        this.bootstrapPreloader();
    }

    
    render() {

        // window.addEventListener('load', () => {
        //     const preloaderElem = document.querySelector('.preloader');
        //     console.log("preloader", preloaderElem)
        //     //
            
        // })

        return (
            <div className="main-container">
                <div className="preloader"></div>
                <div className="w-100 bg-img-specs blue-cloud-bg height1336">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-12 col-md-12">
                                <Header fallDown={this.fallDown}/>
                            </div>
                        </div>
                    </div>
                    <div className="">
                        <Home fallDown={this.fallDown}/>
                    </div>
                </div>

                <About/>
                <Dowedo/>
                <Who/>
                <Havewedone/>
                <Clients/>
                <Contact/>
            </div>
        );
    }
}

ReactDOM.render(<App/>, document.querySelector('#root'));