import React from 'react';
import cloud from '../../img/cloud.png';
import cloudImg from '../../img/cloud.png';

import { Cloud } from './nature/Cloud'

export class About extends React.Component {

    constructor() {
        super();
    }

    render() {
        return(
            <section id="about"  className="text-left position-relative bg-img-specs about-background">
                <div className="container">
                    <div className="row">
                        <div className="col-md-5">
                            <h2 className="section-title" style={{
        fontFamily: "LeagueSpartan"}}>ABOUT</h2>
                            <span className="section-subtitle" style={{
        fontFamily: "LeagueSpartan", color: "#5dadd3"}}>cloud 9</span>
                            <Cloud x={150} y={140} w={80} img={ cloudImg }/>
                            <Cloud x={-100} y={-10} w={200} img={ cloudImg }/>
                            <Cloud x={40} y={70} w={120} img={ cloudImg }/>
                        </div>
                        <div className="col-md-7">
                            <div className="maxw-500">
                                <div className="big-square-block">
                                    <h3 style={{
        fontFamily: "LeagueSpartan"}}>IDEAS</h3>
                                    <h3 className="text-red"style={{
        fontFamily: "LeagueSpartan"}}>ARE BETTER</h3>
                                    <h2 style={{
        fontFamily: "LeagueSpartan"}}>UP HERE</h2>
                                </div>
                                <div className="section-description">
                                    <p>
                                        An Advertising Agency based in San Juan, Puerto Rico. 1/3 of Acosta Enterprises. 
                                        And the most interesting one to work with.
                                    </p>
                                    <p>
                                        We’re your middleperson in selling & brand recognition. Not as easy as it sounds. 
                                        But we make it easy for you.
                                    </p>
                                    <p>
                                        As a team, we let ideas flow to get to the best strategies. Expressing and getting 
                                        to know ourselves has led us to promote a dynamic company culture. Making it a fun 
                                        environment. This has helped us manifest healthy relationships with our clients. 
                                        Ensuring everything gets done in the best fashion possible.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}