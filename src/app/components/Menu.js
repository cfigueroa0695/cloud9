import React from 'react';


export class Menu extends React.Component {

    constructor() {
        super();
    }

    render() {
        return (
            <section id="menu" className="text-left position-relative bg-img-specs about-background">
                <div className="container">
                    <div className="row">
                        <div className="col-md-5">
                            <div className="vertical-menu" style={{
                                display: "inline-grid",
                                padding: "40px",
                                textAlign: "right",
                                fontSize:"210%"
                            }}>
                                <a href="#" class="active">Branding</a>
                                <a href="#">Advertising</a>
                                <a href="#">Digital Design</a>
                                <a href="#">Video</a>
                                <a href="#">Photography</a>
                                </div>
                            </div>
                        </div>
                    </div>
            </section>

        )
    }
}
