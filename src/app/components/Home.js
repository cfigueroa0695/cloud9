import React from 'react';
import PropTypes from 'prop-types';

// IMAGES
import logo from '../../img/cloud9-logo.png';
import pointUp from '../../img/point-up.png';
import cloud from '../../img/cloud.png';
import imgArrowDown from '../../img/arrow-down.png';
import cloudImg from '../../img/cloud.png';

// Components
import { Cloud } from './nature/Cloud'

export class Home extends React.Component {

    constructor(props) {
        super();
        this.state = {
            
        }
   
    }

        render() {            
        
            return (
                <section id="home">
                <div>
                    <img className="img-center" src={ logo } width="300"/>
                    

                    <div className="goto-container">
                        <a id="arrow-down" onClick={(e, elem) => this.props.fallDown(e, '#about')} href="#">
                            <img className="img-center arrow-down" src={ imgArrowDown } width="30"/>
                        </a>
                        <img className="img-center finger-up" src={ pointUp } width="30"/>
                    </div>
                    <Cloud x={200} y={200} w={400} dx={0.3} img={ cloudImg }/>
                    <Cloud x={50} y={250} w={200} dx={0.2} img={ cloudImg }/>
                </div>
                <div id="" className="height480">
                    <Cloud x={50} y={250} w={300} dx={0.3} img={ cloudImg }/>

                    <Cloud x={200} y={80} w={90} dx={0.2} img={ cloudImg }/>

                    <Cloud x={300} y={120} w={350} dx={0.3} img={ cloudImg }/>

                    <Cloud x={250} y={100} w={130} dx={0.2} img={ cloudImg }/>

                    <Cloud x={520} y={150} w={200} dx={0.2} img={ cloudImg }/>

                    <Cloud x={80} y={60} w={150} dx={0.4} img={ cloudImg }/>

                    <Cloud x={50} y={-50} w={150} dx={0.2} img={ cloudImg }/>

                     <Cloud x={90} y={-100} w={120} dx={0.1} img={ cloudImg }/>

                     <Cloud x={-50} y={200} w={190} dx={0.3} img={ cloudImg }/>

                     <Cloud x={-100} y={-150} w={150} dx={0.4} img={ cloudImg }/>

                     <Cloud x={210} y={350} w={130} dx={0.2} img={ cloudImg }/>

                    <Cloud x={520} y={300} w={200} dx={0.2} img={ cloudImg }/>

                    <Cloud x={80} y={400} w={150} dx={0.4} img={ cloudImg }/>

                 <Cloud x={50} y={380} w={200} dx={0.2} img={ cloudImg }/>





                 
                 <Cloud x={-90} y={100} w={164} dx={0.1} img={ cloudImg }/>

                 <Cloud x={-40} y={250} w={50} dx={0.3} img={ cloudImg }/>

                 <Cloud x={40} y={50} w={91} dx={0.2} img={ cloudImg }/>

                 <Cloud x={180} y={30} w={30} dx={0.2} img={ cloudImg }/>

                 <Cloud x={-10} y={-30} w={80} dx={0.2} img={ cloudImg }/>

                 <Cloud x={10} y={-130} w={80} dx={0.3} img={ cloudImg }/>

                 <Cloud x={410} y={-300} w={130} dx={0.3} img={ cloudImg }/>

                 <Cloud x={130} y={-400} w={100} dx={0.2} img={ cloudImg }/>

                  <Cloud x={60} y={500} w={50} dx={0.2} img={ cloudImg }/>

                   <Cloud x={-160} y={450} w={90} dx={0.3} img={ cloudImg }/> 
                </div>
                </section>
            );
    }
}


