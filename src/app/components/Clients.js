import React from 'react';
import ClientsSlider from 'react-slick';
// import { Manager, Target, Popper, Arrow } from 'react-popper'
import Popper from "popper.js"

// IMAGES
import homePatio from '../../img/home-patio.png';
import lasBrisas from '../../img/las-brisas.png';
import italian from '../../img/italian logo.png';
import caribe from '../../img/caribe prueba.png';
import { DigitalDesign } from './DigitalDesign';
import { HomePatioClient } from './HomePatioClient';
import { BrisasClient } from './BrisasClient';
import { CaribeClient } from './CaribeClient';
import { ItalianClient } from './ItalianClient';



import { Home } from './Home';


const settings = {
  dots: false,
  inifinite: true,
  speed: 500,
  slidesToShow: 4,
  slidesToScroll: 0,
  prevArrow: false,
  nextArrow: false,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        dots: true,
        inifinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: false,
        nextArrow: false,
      }
    }
  ]
}


export class Clients extends React.Component {
  constructor() {
    super();

    this.state = {
      homePatioClicked: false,
      caribeClicked: false,
      brisasClicked: false, 
      italianClicked: false
    }

  }
  handleHomePatioClick = (event) => {
    event.preventDefault();

    this.setState({ homePatioClicked: true });
  }

  handleCaribeClick = (event) => {
    event.preventDefault();

    this.setState({ caribeClicked: true });
  }

  handleBrisasClick = (event) => {
    event.preventDefault();

    this.setState({ brisasClicked: true });
  }

  handleItalianClick = (event) => {
    event.preventDefault();

    this.setState({ italianClicked: true });
  }
  showView = () => {
    if(!this.state.homePatioClicked && !this.state.caribeClicked && !this.state.brisasClicked && !this.state.italianClicked){
      return (
        <section id="clients" className="bg-img-specs light-cloud">
          <div className="container-fluid">
            <div className="row">
              <div className="col-md-12">
                <h2 className="section-title" style={{paddingLeft: "30px",
               fontFamily: "LeagueSpartan",
               paddingTop: "35px",
               fontSize: "80px"}}>CLIENTS</h2>
              </div>
            </div>
          </div>
  
          <div className="w-100 mar-bot100 mar-top100 bg-white">
            <ClientsSlider {...settings}>
  
  
              <img  href="#" onClick={(e) => this.handleHomePatioClick(e)} src={homePatio} className="popItem-1" width="400" style={{
                paddingTop: "5%"
              }} />
  
             <div><img href="#" onClick={(e) => this.handleCaribeClick(e)} src={caribe} className="popItem-1" style={{
           paddingTop: "15%",
           paddingLeft: "85px"
              }} /></div>
  
              <img  onClick={(e) => this.handleBrisasClick(e)} src={lasBrisas} className="popItem-2" width="400" style={{
                paddingTop: "5%"
              }} />
  
              <div><img  onClick={(e) => this.handleItalianClick(e)} src={italian} className="popItem-2" style={{
                paddingTop: "15%"
              }} /></div>
  
  
            </ClientsSlider>
          </div>
  
        </section>
        
      );
    }else if(this.state.homePatioClicked && !this.state.caribeClicked && !this.state.brisasClicked && !this.state.italianClicked){
       return(<HomePatioClient />);
    }else if(!this.state.homePatioClicked && this.state.caribeClicked && !this.state.brisasClicked && !this.state.italianClicked){
      return(<CaribeClient />);
  }else if(!this.state.homePatioClicked && !this.state.caribeClicked && this.state.brisasClicked && !this.state.italianClicked){
    return(<BrisasClient />);
  }else{
    return(<ItalianClient />);
  }
}


  render() {
    return(
    <div>{this.showView()}</div>
    )
  }
}
































// showPopper(e) {
//     // let target = document.querySelector('target');
//     let popTooltip = e.target.nextElementSibling
//     // console.log('popper',e.target.nextSibling)

//     popTooltip.style.display = 'block';
// }

// hidePopper(e) {
//     // let target = document.querySelector('target');
//     let popTooltip = e.target.nextElementSibling
//     popTooltip.style.display = 'none';

// }

// componentDidMount() {
//     let popTooltip = document.querySelector(`.client-popper`)
//     popTooltip.style.display = 'none';
// }







{/* <Manager>
                    <Target className="bg-img-specs img-home-patio"
                        onMouseOver={(e) => this.showPopper(e)}
                        onMouseLeave={(e) => this.hidePopper(e)}>
                    </Target>
                    <Popper placement="top" className="popper client-popper" 
                    style={{width: '100%', height: '400px', backgroundColor: '#000', color: '#fff'}}>
                        Right Content
                    <Arrow className="popper__arrow"/>
                    </Popper>
                    <Popper placement="right" className="popper client-popper" 
                    style={{width: '100%', height: '400px', backgroundColor: '#000', color: '#fff'}}>
                        Right Content
                    <Arrow className="popper__arrow"/>
                    </Popper>
                </Manager>
                <Manager>
                    <Target className="bg-img-specs img-las-brisas"
                        onMouseOver={(e) => this.showPopper(e)}
                        onMouseLeave={(e) => this.hidePopper(e)}>
                    </Target>
                    <Popper placement="left" className="popper client-popper" 
                    style={{width: '100%', height: '400px', backgroundColor: '#000', color: '#fff'}}>
                        Left Content
                    <Arrow className="popper__arrow"/>
                    </Popper>
                </Manager>  */}