import React from 'react';

// IMAGES
import imgWorld from '../../img/world.png';
import imgMoon from '../../img/moon.png';
import imgCloud from '../../img/cloud.png';
import imgTurtle from '../../img/turtle.png';


export class Contact extends React.Component {
    constructor() {
        super();
    }

    render() {
        return (
            <section id="contact" className="bg-big-cloud bg-img-specs">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            <h2 className="section-title" style={{
        fontFamily: "LeagueSpartan",
        paddingTop: "50px",
        fontSize: "70px" }}>CONTACT</h2>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="world-img-container">
                        <img src={ imgWorld }/>
                    </div>
                    <div className="moon-img-container">
                        <img src={ imgMoon }/>
                    </div>
                    <div className="row">
                        <div className="col-md-5 offset-md-7">
                            <form className="contact-form">
                                <input type="text" name="name" className="form-control" placeholder="Name"/>
                                <input type="text" name="email" className="form-control" placeholder="Email Address"/>
                                <input type="text" name="subject" className="form-control" placeholder="Subject"/>
                                <textarea name="message" className="form-control" placeholder="Your message"/>
                                <button className="btn btn-submit"> Submit </button>
                            </form>
                        </div>
                        <div className="cloud-turtle-container">
                            <img src={ imgCloud } className="contact-cloud"/>
                            <img src={ imgTurtle } className="contact-turtle"/>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}