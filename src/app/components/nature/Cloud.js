import React from 'react';
import ReactDOM from 'react-dom'
import cloudImg from '../../../img/cloud.png';


export class Cloud extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            x: props.x,
            y: props.y,
            w: props.w,
            dx: props.dx,
            img: props.img
        }
        
    }

    animatedCloud() {
        requestAnimationFrame(this.animatedCloud.bind(this))
        
        if(this.state.x + this.state.w > window.innerWidth || this.state.x < 0) {

            this.setState({
                dx: -this.state.dx
            })
        }

        this.setState({
            x: this.state.x += this.state.dx 
        })
        
    }

    componentDidMount() {
      this.animatedCloud();

      setTimeout(() => {
          console.log(this.state.dx)
      }, 3000);
    }


    render() {
        
        return (
            <img ref="imgAnimated" src={ this.state.img } style={ { 
                transform: `translate(${this.state.x}px, ${this.state.y}px)`, 
                width: `${this.state.w}px`
            } }/>
        )
    }
}