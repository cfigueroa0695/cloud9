import React from 'react';
import cloud from '../../img/cloud.png';
import cloudImg from '../../img/cloud.png';
import {Branding} from './Branding';
import {DigitalDesign} from './DigitalDesign';
import {Photography} from './Photography';
import {Advertising} from './Advertising';
import imgVideo from "../../img/video image.jpg";
import {Animated} from "react-animated-css";
import imgPhotography from "../../img/photography.jpg";


import { Cloud } from './nature/Cloud'

export class Video extends React.Component {

    constructor() {
        super();

        this.state = {
            showMenu: false,
            clicked: false,
            branding:false,
            advertising: false,
            digitalDesign: false,
            photography: false,
            video: false
      
          }
        }
      
        translateUp(e) {
          e.preventDefault();
          let menu = document.querySelector('.hidden-menu');
      
          this.setState({
            showMenu: true
          })
      
          if (this.state.showMenu) {
            menu.style = "transform: translateY(0);"
          }
      
        }
        handleClick(e) {
          this.setState({
            clicked: true
          })
          console.log(e.target.name);
        }
      
        handleBranding = (event) => {
          event.preventDefault();
      
          this.setState({branding:true});
        }
      
        handleAdvertising = (event) => {
          event.preventDefault();
      
          this.setState({advertising:true});
        }
      
        handleDigitalDesign = (event) => {
          event.preventDefault();
      
          this.setState({digitalDesign:true});
        }
      
        handleVideo = (event) => {
          event.preventDefault();
      
          this.setState({video:true});
        }
        handlePhotography = (event) => {
          event.preventDefault();
      
          this.setState({photography:true});
        }

        showView = () => {
            if (!this.state.branding && !this.state.advertising && !this.state.photography && !this.state.video && !this.state.digitalDesign) {
            return(
            <section id="branding" className="text-left position-relative bg-img-specs about-background">
            <div className="container">
                <div className="row">
                    <div className="col-md-5">
                    <h2 className="section-title" style={{
               fontFamily: "LeagueSpartan",paddingTop: "50px"
              }}>WHAT</h2>
                      <span className="section-subtitle" style={{
               fontFamily: "LeagueSpartan",  color: "#5dadd3"
              }}>have we done?</span>
                        <div className="vertical-menu" style={{
                            display: "inline-grid",
                            padding: "40px",
                            textAlign: "right",
                            fontSize:"210%"
                        }}>
                          <a href="#" style={{paddingBottom: "20px",
fontSize: "15px", color:"black", fontFamily: "LeagueSpartan"}} onClick={(e) => this.handleBranding(e)}>BRANDING</a>
                  <a href="#" style={{paddingBottom: "20px",
fontSize: "15px", fontFamily: "LeagueSpartan", color:"black" }} onClick={(e) => this.handleAdvertising(e)}>ADVERTISING</a>
                  <a href="#" style={{paddingBottom: "20px",
fontSize: "15px", color:"black", fontFamily: "LeagueSpartan" }} onClick={(e) => this.handleDigitalDesign(e)}>DIGITAL DESIGN</a>
                  <a href="#" style={{paddingBottom: "20px",
fontSize: "15px", color:"black", fontFamily: "LeagueSpartan"}} onClick={(e) => this.handleVideo(e)}>VIDEO + PHOTOGRAPHY</a>
                            </div>
                    </div>
                    <div className="col-md-7">
                        <div className="maxw-500" style={{
       paddingTop: "50px"}}>
                            <div className="big-square-block">
                            <h3 className="text-red" style={{
               fontFamily: "LeagueSpartan"
              }}>VI</h3>
                            <h3 style={{
               fontFamily: "LeagueSpartan"
              }}>DEO</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

             <div className="col-md-12" align="center" style={{ backgroundColor: "#37c3e4", paddingTop: "60px",
paddingBottom: "60px"}} >
 <img src={imgPhotography} className="popItem-1" onClick={(e) => this.handlePhotography(e)} width="200" style={{
                            float: "right"
              }}/>
            <center><img src={imgVideo} className="popItem-1"  width="500"/></center>
                

            </div>
        </section>
            );
        
        }else if(!this.state.branding && this.state.advertising && !this.state.photography && !this.state.video && !this.state.digitalDesign){
            return(<Advertising/>);
        }else if(!this.state.branding && !this.state.advertising && this.state.photography && !this.state.video && !this.state.digitalDesign){
           return(<Photography/>);
        }else if(!this.state.branding && !this.state.advertising && !this.state.photography && this.state.video && !this.state.digitalDesign){
           return(<Video/>); 
        }else if(!this.state.branding && !this.state.advertising && !this.state.photography && !this.state.video && this.state.digitalDesign){
            return(<DigitalDesign/>);
        } else {
            return(<Branding/>);
        }
   }

    render() {
        return (
           <div>{this.showView()}</div> 
        )
    }
}


