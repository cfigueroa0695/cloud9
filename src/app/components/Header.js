import React from 'react';

// IMAGES
import imgLogo from '../../img/cloud9-logo.png'

export class Header extends React.Component {
    
    constructor() {
        super();
        this.state = {
            visible: false
        }

        this.isHidden = this.isHidden.bind(this)
    }
    
    changeColor(e) {
        // e.target.parentElement.style.backgroundColor = 'black';
    }

    isHidden() {
        // console.log('scrolled...')
        let winVertPos = Math.floor(window.scrollY);
        let headerHeight = document.querySelector('header').clientHeight;

        if(winVertPos > headerHeight) {

            this.setState({
                visible: true
            })
        
        } 
        
        if (winVertPos === 0) {

            this.setState({
                visible: false
            })

        }
    }

    componentDidMount() {
        this.isHidden()
    }



    render(){

        window.addEventListener('scroll', this.isHidden)

        return (
            <div>
                <header className="row-flex pos-fixed top20 space-btw">
                    <div className="logo" style={ { visibility: this.state.visible ? 'visible' : 'hidden' }  }>
                        <img src={ imgLogo } width="130"/>
                    </div>
                    <div className="hamburger-container">
                        <nav className="">
                            <div className="text-right">
                                <button className="navbar-toggler brand-blue" type="button" data-toggle="collapse" 
                                        data-target="#toggleNavMenu" aria-controls="navbarToggleExternalContent" 
                                        aria-expanded="false" aria-label="Toggle navigation" onClick={(e) => this.changeColor(e)}>
                                    <span className="fa fa-bars fa-2x"></span>
                                </button>
                            </div>
                            <div className="collapse brand-blue-light" id="toggleNavMenu">
                                <div className="navbar-header p-4">
                                    <ul>
                                        <li><a href="#" onClick={(e, elem) => this.props.fallDown(e, '#about')}>WHAT</a></li>
                                        <li>
                                            <a href="#" onClick={(e, elem) => this.props.fallDown(e, '#dowedo')}>
                                                WHAT<br/>
                                            <small>dowedo</small>
                                            </a>
                                        </li>
                                        <li><a href="#" onClick={(e, elem) => this.props.fallDown(e, '#who')}>WHO</a></li>
                                        <li>
                                            <a href="#" onClick={(e, elem) => this.props.fallDown(e, '#havewedone')}>
                                                WHAT<br/>
                                            <small>have we done</small>
                                            </a>
                                        </li>
                                        <li><a href="#" onClick={(e, elem) => this.props.fallDown(e, '#clients')}>CLIENTS</a></li>
                                        <li><a href="#" onClick={(e, elem) => this.props.fallDown(e, '#')}>CONTACT</a></li>
                                    </ul>
                                </div>
                            </div>
                        </nav>
                    </div>
                </header>
            </div>    
        );
    }

}