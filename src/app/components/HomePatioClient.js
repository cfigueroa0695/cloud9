import React from 'react';
import ClientsSlider from 'react-slick';
// import { Manager, Target, Popper, Arrow } from 'react-popper'
import Popper from "popper.js"

// IMAGES
import homePatio from '../../img/home-patio.png';
import lasBrisas from '../../img/las-brisas.png';
import italian from '../../img/logo italian awnings.png';
import caribe from '../../img/logo caribe co work.png';
import cuadro from '../../img/home +patio barra.jpg';
import flecha from '../../img/back cloud9.png';
import { Clients } from './Clients';

const settings = {
    dots: false,
    inifinite: true,
    speed: 500,
    slidesToShow: 2,
    slidesToScroll: 0,
    prevArrow: false,
    nextArrow: false,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                dots: true,
                inifinite: true,
                speed: 500,
                slidesToShow: 1,
                slidesToScroll: 1,
                prevArrow: false,
                nextArrow: false,
            }
        }
    ]
}



export class HomePatioClient extends React.Component {
        constructor() {
            super();
        
            this.state = {
              homePatioClicked: false
            }
        
          }
          handleHomePatioClick = (event) => {
            event.preventDefault();
        
            this.setState({ homePatioClicked: true });
          }

    

 
       
        
        
          showView = () => {
            if(!this.state.homePatioClicked){
        return (
            <section id="clients" className="bg-img-specs light-cloud">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            <h2 className="section-title" style={{paddingLeft: "30px",
               fontFamily: "LeagueSpartan",
               paddingTop: "35px",
               fontSize: "80px"}}>CLIENTS</h2>
                        </div>
                    </div>
                </div>
                
                <div className="w-100 mar-bot100 mar-top100 bg-white">

                <img src={ flecha } onClick={(e) => this.handleHomePatioClick(e)} style={{
                            width: "30px",
                            height: "30px",
                            marginTop: "2%",
                            marginLeft: "2%"
                        }}  />
                    <ClientsSlider { ...settings }>

                 
                 
                 <img href="#" onClick={(e) => this.handleHomePatioClick(e)} src={ cuadro } className="popItem-1"  style={{
                            paddingTop: "5%"
                        }} />

                        
         
                    </ClientsSlider>
                </div>
                
            </section>
        );
    }else{
        return(<Clients />)
    }
          }
        
    render() {
        return(
            <div>{this.showView()}</div>
            )
}
}
