import React from 'react';

// IMAGES
import computerSearch from "../../img/computer-search.png";
import envelope from "../../img/envelope.png";
import messageLike from "../../img/message-like.png";
import megaphone from "../../img/megaphone.png";
import essentials from "../../img/essentials.png";
import magnifier from "../../img/magnifier.png";
import shoppers from "../../img/shoppers.png";
import magazine from "../../img/magazine.png";
import newspaper from "../../img/newspaper.png";
import gorilla from "../../img/gorilla.png";
import ad from "../../img/ad.png";
import desktop from "../../img/desktop.png";
import radio from "../../img/radio.png";
import cloud from '../../img/cloud.png';
import cloudImg from '../../img/cloud.png';

import { Cloud } from './nature/Cloud'

export class Dowedo extends React.Component {
    constructor() {
        super();

    }

    render() {
        return (
            <section id="dowedo" className="mt-30px bg-img-specs side-cloud">
                <div className="container">
                    <div className="row">
                        <div className="col-md-5">
                            <h2 className="section-title" style={{
        fontFamily: "LeagueSpartan"}}>WHAT</h2>
                            <span className="section-subtitle" style={{
        fontFamily: "LeagueSpartan",  color: "#5dadd3"}}>dowedo?</span>
                            <Cloud x={210} y={50} w={100} img={ cloudImg }/>
                            <Cloud x={-100} y={50} w={150} img={ cloudImg }/>
                            <Cloud x={50} y={100} w={250} img={ cloudImg }/>
                        </div>
                        <div className="col-md-7">
                            <div className="maxw-550">
                                <div className="big-rectangle-block">
                                    <div className="container">
                                        <h3 className="text-center"style={{
        fontFamily: "LeagueSpartan"}}>DIGITAL</h3>
                                        <div className="row">
                                            <div className="rectangle-item">
                                                <img src={ computerSearch } className="maxw-100"/>
                                                <h4 className="text-center">Analytics</h4>
                                            </div>
                                            <div className="rectangle-item">
                                                <img src={ envelope } className="maxw-100"/>
                                                <h4 className="">Email Campaings</h4>
                                            </div> 
                                            <div className="rectangle-item">
                                                <img src={ messageLike } className="maxw-100"/>
                                                <h4 className="">Social Media <br/> Management</h4>
                                            </div> 
                                            <div className="rectangle-item">
                                                <img src={ megaphone } className="maxw-80"/>
                                                <h4 className="">Google Ads</h4>
                                            </div> 
                                            <div className="rectangle-item">
                                                <img src={ essentials } className="maxw-160"/>
                                                <h4 className="">Websites, Mobiles & Apps</h4>
                                            </div> 
                                            <div className="rectangle-item">
                                                <img src={ magnifier } className="maxw-55"/>
                                                <h4 className="">Search Engine <br/> Optimization</h4>
                                            </div>                                             
                                        </div>
                                        <h3 className="text-center" style={{
        fontFamily: "LeagueSpartan"}}>TRADITIONAL MARKETING</h3>
                                        <div className="row">
                                            <div className="rectangle-item">
                                                <img src={ shoppers } style={{maxWidth: 60}}/>
                                                <h4 className="">Shoppers</h4>
                                            </div>                       
                                            <div className="rectangle-item">
                                                <img src={ magazine } style={{maxWidth: 90}}/>
                                                <h4 className="">Magazine</h4>
                                            </div>                       
                                            <div className="rectangle-item">
                                                <img src={ newspaper } style={{maxWidth: 100}}/>
                                                <h4 className="">Newspaper</h4>
                                            </div>
                                        </div>
                                        <div className="row">                       
                                            <div className="rectangle-item flex-25">
                                                <img src={ gorilla } style={{maxWidth: 60}}/>
                                                <h4 className="">Guerrilla</h4>
                                            </div>                       
                                            <div className="rectangle-item flex-25">
                                                <img src={ ad } style={{maxWidth: 110}}/>
                                                <h4 className="">Billboards</h4>
                                            </div>                       
                                            <div className="rectangle-item flex-25">
                                                <img src={ desktop } style={{maxWidth: 110}}/>
                                                <h4 className="">Television</h4>
                                            </div>                       
                                            <div className="rectangle-item flex-25">
                                                <img src={ radio } style={{maxWidth: 100}}/>
                                                <h4 className="">Radio</h4>
                                            </div>                       
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }


}