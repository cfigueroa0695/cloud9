import React from 'react';
import { Branding } from './Branding';
import { Advertising } from './Advertising';
import { DigitalDesign } from './DigitalDesign';
import { Photography } from './Photography';
import { Video } from './Video';
import { Animated } from "react-animated-css";


// Images
import imgPoolPatio from "../../img/pool-patio.jpg";
import imgDesigner from "../../img/designer.jpg";
import imgCamera from "../../img/camera.jpg";
import imgCaribe from "../../img/caribe.jpg";

export class Havewedone extends React.Component {
  constructor() {
    super();

    this.state = {
      showMenu: false,
      clicked: false,
      branding: false,
      advertising: false,
      digitalDesign: false,
      photography: false,
      video: false

    }
  }

  translateUp(e) {
    e.preventDefault();
    let menu = document.querySelector('.hidden-menu');

    this.setState({
      showMenu: true
    })

    if (this.state.showMenu) {
      menu.style = "transform: translateY(0);"
    }

  }
  handleClick(e) {
    this.setState({
      clicked: true
    })
    console.log(e.target.name);
  }

  handleBranding = (event) => {
    event.preventDefault();

    this.setState({ branding: true });
  }

  handleAdvertising = (event) => {
    event.preventDefault();

    this.setState({ advertising: true });
  }

  handleDigitalDesign = (event) => {
    event.preventDefault();

    this.setState({ digitalDesign: true });
  }

  handleVideo = (event) => {
    event.preventDefault();

    this.setState({ video: true });
  }
  handlePhotography = (event) => {
    event.preventDefault();

    this.setState({ photography: true });
  }


  showView = () => {
    if (!this.state.branding && !this.state.advertising && !this.state.photography && !this.state.video && !this.state.digitalDesign) {
      return (
        <section id="havewedone">
        <div className="projects">
          <div className="container-fluid">
            <div className="row">
              <div className="col-md-12">
                <h2 className="section-title" style={{paddingLeft: "50px", paddingTop:"50px",
        fontFamily: "LeagueSpartan"}}>WHAT</h2>
                <span className="section-subtitle" style={{maxWidth: "500px",
        fontFamily: "LeagueSpartan",  color: "#5dadd3"}}>have</span>
         <span className="section-subtitle" style={{maxWidth: "500px",
        fontFamily: "LeagueSpartan",  color: "#5dadd3"}}>we done?</span>
              </div>
            </div>
          </div>
        <div className="project-squares">
          <div className="row odd-bg-blue">
            <div className="col-md-3 square">
              <span className="">
                <a id="project-branding-link" href="#project-branding"
                  role="tab" aria-controls="project-branding" aria-selected="true"
                  onClick={(e) => this.handleBranding(e)}>
                  <h3>Branding</h3>
                </a>
              </span>
            </div>
            <div className="col-md-3 square bg-img-specs">

            </div>
            <div className="col-md-3 square">
              <span>
                <a id="project-advertising-link" href="#project-advertising"
                  role="tab" aria-controls="project-advertising" aria-selected="false"
                  onClick={(e) => this.handleAdvertising(e)}>
                  <h3>Advertising</h3>
                </a>
              </span>
            </div>
            <div className="col-md-3 square bg-img-specs">

            </div>
          </div>
          <div className="row even-bg-blue wrap-reverse">
            <div className="col-md-3 square bg-img-specs">

            </div>
            <div className="col-md-3 square">
              <span>
                <a id="project-digital-link" href="#project-digital"
                  role="tab" aria-controls="project-digital" aria-selected="false"
                  onClick={(e) => this.handleDigitalDesign(e)}>
                  <h3>Digital</h3>
                  <h3>Design</h3>
                </a>
              </span>
            </div>
            <div className="col-md-3 square bg-img-specs">

            </div>
            <div className="col-md-3 square">
              <span>
                <a href="#project-video" onClick={(e) => this.handlePhotography(e)}>
                  <h3>Video</h3>
                  <h3>+</h3>
                  <h3>Photography</h3>
                </a>
              </span>
            </div>
          </div>
        </div>
          </div> 
          </section>

        
      );
    } else if (this.state.branding && !this.state.advertising && !this.state.photography && !this.state.video && !this.state.digitalDesign) {
      return (<Animated animationIn="fadeIn" animationOut="fadeOut" isVisible={true}><Branding /></Animated>);
    } else if (!this.state.branding && this.state.advertising && !this.state.photography && !this.state.video && !this.state.digitalDesign) {
      return (<Animated animationIn="fadeIn" animationOut="fadeOut" isVisible={true}><Advertising /></Animated>);
    } else if (!this.state.branding && !this.state.advertising && this.state.photography && !this.state.video && !this.state.digitalDesign) {
      return (<Animated animationIn="fadeIn" animationOut="fadeOut" isVisible={true}><Photography /></Animated>);
    } else if (!this.state.branding && !this.state.advertising && !this.state.photography && this.state.video && !this.state.digitalDesign) {
      return (<Animated animationIn="fadeIn" animationOut="fadeOut" isVisible={true}><Video /></Animated>);
    } else {
      return (<Animated animationIn="fadeIn" animationOut="fadeOut" isVisible={true}><DigitalDesign /></Animated>);
    }
  }
  render() {
    return (
      
            <div>{this.showView()}</div>
        
    )
  }
}