import React from 'react';
import TeamSlider from 'react-slick';

// IMAGES
import person1 from "../../../img/person1.png";
import person2 from "../../../img/person2.png";
import person3 from "../../../img/person3.png";
import person4 from "../../../img/person4.png";


import CustomPaging from './CustomPaging'

export class Who extends React.Component {
    constructor() {
        super();

        this.state = {
            names: ["Gabriel","Jorge","Mari","Francisco"],
            notActive: '',
            componentMounted: false
        }
        
    }
    

    isDotNav(e) {
        if (!e.target.matches('.person-nav')) {
            return;
        } else {

            this.addOpacityClass();
        }
    }

    
    
    addOpacityClass() { 
        
        let slickDotsParent = document.querySelector('.slick-dots');
        let slickDotsChilds = slickDotsParent.children;
        let listDotsArr = Array.prototype.slice.call( slickDotsChilds )

        console.log("slickdotsparent", slickDotsParent)
        console.log("slickdotschilds", slickDotsChilds)
        console.log("listdoarr", listDotsArr)

        listDotsArr.map((item, i) => {
            
            if(item.getAttribute('class') != 'slick-active') {

                this.setState({ notActive: 'not-active' }, () => {
                    item.setAttribute('class', this.state.notActive)
                })
                
            }

        })
        
    }

    CustomPaging(i) {
        return (
            <a>
                <img src={ require("../../../img/face" + (i + 1) +".png") } className="person-nav"/>
                <span> { this.state.names[i] } </span>
            </a>
        )
    }

    componentDidMount() {
        // this.addOpacityClass()
    }

    render() {

        const settings = {
            dots: true,
            inifinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            prevArrow: false,
            nextArrow: false,
            
            
            customPaging: (index, slick) => {
                return this.CustomPaging(index)
            },
            responsive: [
                {
                  breakpoint: 1024,
                  settings: {
                    dots: true,
                    inifinite: true,
                    speed: 500,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    prevArrow: false,
                    nextArrow: false,
                  }
                }
              ]
              
        }
    
        return (
           
            <section id="who" className="bg-img-specs whoback" onMouseUp={(e) => this.isDotNav(e)}>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            <h2 className="section-title " style={{paddingLeft: "150px", paddingLeft: "50px",
        fontFamily: "LeagueSpartan",
        paddingTop: "50px"}}>WHO</h2>
                            <span className="section-subtitle" style={{maxWidth: "500px", paddingLeft: "50px",
        fontFamily: "LeagueSpartan",
        color: "#5dadd3"}}>works</span>
        <span className="section-subtitle" style={{maxWidth: "500px", paddingLeft: "150px",
        fontFamily: "LeagueSpartan",
        color: "#5dadd3"}}>in cloud 9?</span>
                        </div>
                    </div>
                </div>
              
                <div className="w-100">
                    <TeamSlider { ...settings }>
                        {/* <img src="../../img/slide1.svg"/> */}
                       
                        <div className="slider-bg-img slide1">
                            <div className="col-md-12 person" style={{marginTop: "25px"}}>
                                <div className="independent-element top250 ">
                                    <img src={ person1 } width="500"/>
                                </div>
                                <div className="person__custom-text"style={{fontSize: "13px"}}>
                                <div className="col-md-12">
                                    <h3>Gabriel Acosta CEO</h3>
                                    <p>
                                        He’s probably eating mofongo relleno de churrasco as you read this. 
                                        This is the guy you have to talk to for new business. Gabriel works 
                                        with the serious stuff and the serious people and speaks to us in 
                                        serious ways. Says he’s old-fashioned but encourages modern techniques 
                                        for the job by mentoring the team to maintain a creative, open and 
                                        productive environment – after all, things need to get done. 
                                        Gabriel has an eye for good branding. His mission is to have great 
                                        brand recognition for every client.
                                    </p>
                                </div>
                                </div>
                            </div>
                    </div>
                        <div className="slider-bg-img slide2">
                            <div className="person">
                                <div className="independent-element top250">
                                    <img src={ person2 } width="500"/>
                                </div>

                                <div className="person__custom-text color-black" style={{fontSize: "13px"}}>
                                    <h3>Jorge Soto Creative</h3>
                                    <p>
                                        Jorge’s a logophile (loves words) and a melomaniac (loves music). 
                                        He likes foreign and classic films. And he’s also an avid 
                                        hip-hop/rap connoisseur…music in general. As our copywriter 
                                        (he’s I’m writing this) and creative, with his eccentric manners, 
                                        Jorge’s always looking where nobody seems to look (or maybe he’s 
                                        just spaced out in a daydream) to cultivate ideas for our team and clients.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="slider-bg-img slide3">
                            <div className="person">
                                <div className="independent-element top270">
                                    <img src={ person3 } width="500"/>
                                </div>
                                <div className="person__custom-textCustom color-black top-minus-100" style={{fontSize: "13px"}}>
                                    <h3>Mari Carmen Controller</h3>
                                    <p>
                                        Our beloved Mari. Indirectly in charge of the whole operation. The most 
                                        responsible one in the team with the most responsibilities. Always 
                                        overseeing all our duties. A truly key team player. You know, because she 
                                        collects the loot. If she’s happy, you’re happy and we’re happy. We thought 
                                        you may know her beforehand since she's the one everyone will encounter anyhow.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="slider-bg-img slide4">
                            <div className="person">
                                <div className="independent-element top250">
                                    <img src={ person4 } width="500"/>
                                </div>
                                <div className="person__custom-text color-black " style={{fontSize: "13px"}}>
                                    <h3>Francisco Rodríguez Graphic Designer</h3>
                                    <p>
                                        Francisco “Cisco” has 20+ yrs. experience as a graphic designer. He’s been drawing
                                        waaay before Pre-school (“Since I could hold my first crayon and paper. I did my 
                                        first doodle/graffiti at the young age of two!”). Cisco loves comic books. He’s 
                                        the co-creator and illustrator of ECOS graphic novel on sale now on ComiXology an 
                                        Amazon Company. Enthusiast of any song by John Williams. Loves Spielberg movies. 
                                        Self-proclaimed “best lasagna eater in the world. And has a decent sense of
                                        humor. He’s a perfectionist for good illustrations and on-point branding. 
                                        Recognizes deadlines + getting costumer approvals…Also, superb at parallel parking.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </TeamSlider>
                   
                </div>
            </section>
          
        )
    }


}