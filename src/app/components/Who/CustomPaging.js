import React from 'react'

export class CustomPaging extends React.Component {
    constructor() {
        super();

        this.state = {
            componentMounted: false
        }
    }

    addOpacity() {
        this.props.addOpacity();
    }

    CustomPaging() {
        return (
            <a>
                <img src={ "../../img/face" + (this.props.index + 1) +".png" }/>
                <span> { this.state.names[this.props.index] } </span>
            </a>
        )
    }

    componentDidMount() {
        this.setState({
            componentMounted: true
        })

        this.addOpacity()
        
    }
    

    render() {
        return this.CustomPaging()
    }
}