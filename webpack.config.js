const webpack = require('webpack')
const path = require('path')
const HtmlPlugin = require('html-webpack-plugin')

const DIST_DIR = path.resolve(__dirname, "dist")
const SRC_DIR = path.resolve(__dirname, "src")

const config = {
    entry: SRC_DIR + '/app/index.js',
    output: {
        path: DIST_DIR + '/app',
        filename: "bundle.js",
        publicPath: '/app'
    },
    module: {
        rules: [
            {
                test: /\.js?/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ["react", "es2015", "stage-2"]
                    }
                }

            },
            {
                test: /\.css$/,
                use: [
                    "style-loader", "css-loader"
                ]
            },
            // {
            //     test: /\.(jpe?g|png|gif|svg)$/,
            //     use: [
            //         'file-loader',
            //         {
            //         loader: 'image-webpack-loader',
            //             options: {
            //                 mozjpeg: {
            //                   enabled: false
            //                 },
            //                 // optipng.enabled: false will disable optipng
            //                 optipng: {
            //                   enabled: false,
            //                 },
            //                 pngquant: {
            //                   quality: '65-90',
            //                   speed: 4
            //                 },
            //                 gifsicle: {
            //                   interlaced: false,
            //                 },
            //                 // the webp option will enable WEBP
            //                 webp: {
            //                   quality: 75
            //                 },
            //                 name: '[path][name]-[hash:8].[ext]'
            //             }
            //         }
            //     ]
            // },

            {
                test: /\.(woff2?|eot|ttf|ttc|otf)(\?.*)?$/,
                use: [
                    'file-loader?name=/fonts/[name].[ext]'
                ]
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 1000000,
                            fallback: 'file-loader'
                        }
                    }
                ]
            },


        ]
    },
    performance: { hints: false },

    plugins: [
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        new HtmlPlugin({
            template: 'src/index.html'
        })
    ]
}

module.exports = config