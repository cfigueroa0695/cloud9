let express    = require('express');
let bodyParser = require('body-parser');
let path       = require('path');
let PORT       = process.env.PORT || 8080;

let app = express();

// Middleware
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'dist')));

app.get('/', (req,res) => {
    res.sendFile(path.resolve(__dirname + '/dist/index.html'));
})


app.listen( PORT, (err) => {
    err ? 
        console.error(err) : 
        console.log(`Rocking on port ${PORT}. Visit http://localhost:${PORT}/ in your browser.`)
})